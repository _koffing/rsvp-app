import React from 'react';
import PropTypes from 'prop-types';

const AttendingFilter = (props) => {
    return (
        <div>
            <h2>Invitees</h2>
            <label>
                <input type="checkbox" onChange={props.toggleFilter} checked={props.isFiltered} /> Hide those who haven't responded
            </label>
        </div>
    );
}

AttendingFilter.propTypes = {
    toggleFilter: PropTypes.func.isRequired,
    isFiltered: PropTypes.bool.isRequired
}

export default AttendingFilter;