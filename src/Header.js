import React from 'react';
import PropTypes from 'prop-types';
import AddGuest from './AddGuest';

const Header = (props) => {
    return (
        <header>
            <h1>RSVP</h1>
            <p>A Treehouse App</p>
            <AddGuest
                pendingGuest={props.pendingGuest}
                updatePendingGuest={props.updatePendingGuest}
                addGuest={props.addGuest}
            />
        </header>
    );
}

Header.propTypes = {
    pendingGuest: PropTypes.string.isRequired,
    updatePendingGuest: PropTypes.func.isRequired,
    addGuest: PropTypes.func.isRequired
}

export default Header;