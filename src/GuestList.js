import React from 'react';
import PropTypes from 'prop-types';
import Guest from './Guest';
import PendingGuest from './PendingGuest';

const GuestList = (props) => {
    return (
        <ul>
            <PendingGuest name={props.pendingGuest} />
            {props.guests.filter(guest => !props.isFiltered || guest.isConfirmed )
            .map((guest, i)=>{
                return(
                    <Guest 
                        name={guest.name} 
                        isConfirmed={guest.isConfirmed} 
                        isEditing={guest.isEditing}
                        key={i} 
                        // pass in an anonomys function that will call toggleConfirmationAt with the index passed to it, the index is maintained by a closure 
                        handleConfirmation={() => { props.toggleConfirmationAt(guest.id)}}
                        handleEditing={() => { props.toggleEditingAt(guest.id)}}
                        setName={text => props.setNameAt(text, guest.id)}
                        handleRemoveGuest={() => { props.removeGuest(guest.id)}}
                        />
                );
            })}
            {/* <li className="pending"><span>Safia</span></li>*/}
        </ul>
    );
}

GuestList.propTypes = {
    guests: PropTypes.array.isRequired,
    toggleConfirmationAt: PropTypes.func.isRequired,
    toggleEditingAt: PropTypes.func.isRequired,
    setNameAt: PropTypes.func.isRequired,
    isFiltered: PropTypes.bool.isRequired,
    removeGuest: PropTypes.func.isRequired,
    pendingGuest: PropTypes.string.isRequired,
}

export default GuestList;