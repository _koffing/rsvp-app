import React from 'react';
import PropTypes from 'prop-types';

const AddGuest = (props) => {
    return (
        <form onSubmit={props.addGuest} > 
            <input type="text" value={props.pendingGuest} placeholder="Invite Someone" onChange={props.updatePendingGuest}/>
            <button type="submit" name="submit" value="submit" >Submit</button>
        </form>
    );
}

AddGuest.propTypes = {
    pendingGuest: PropTypes.string.isRequired,
    updatePendingGuest: PropTypes.func.isRequired,
    addGuest: PropTypes.func.isRequired
}

export default AddGuest;