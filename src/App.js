import React, {Component} from 'react';
import Header from './Header';
import AttendingFilter from './AttendingFilter';
import GuestList from './GuestList';
import Counter from './Counter';

class App extends Component {
    constructor(props){
        super(props);
        this.lastId = 0;
        this.state = {
            isFiltered: false,
            pendingGuest: '',
            attendingGuests: 0, 
            unconfirmedGuests: 0,
            guests: [
            ],
        };
    }

    toggleFilter = () => {
        this.setState({ isFiltered: !this.state.isFiltered});
    }

    updatePendingGuest = (e) => {
        this.setState({ pendingGuest: e.target.value });
    }

    setId = (lastId) => {
        this.lastId = this.lastId + 1;
        return this.lastId;
    }

    addGuest = (e) => {
        e.preventDefault();
        this.setState({
            guests: [
                {
                    id: this.setId(this.lastId),
                    name: this.state.pendingGuest,
                    isConfirmed: false,
                    isEditing: false
                },
                ...this.state.guests
            ],
            pendingGuest: ''
        });
    }

    removeGuest = (id) => {
        this.setState({
            guests: this.state.guests.filter(guest => id !== guest.id)
        });
    }

    getTotalInvited = () => this.state.guests.length;

    toggleGuestPropertyAt = (property, id) => {
        this.setState({
            guests: this.state.guests.map((guest) => {
                if (id === guest.id) {
                    return {
                        ...guest,
                        [property]: !guest[property]
                    }; 
                }
                return guest;
            })
        });
    }
    
    toggleConfirmationAt = (id) => {
        this.toggleGuestPropertyAt('isConfirmed', id);
        this.setAttending();
    }

    toggleEditingAt = (id) => {
        this.toggleGuestPropertyAt('isEditing', id);
    }

    setNameAt = (name, id) => {
        this.setState({
            guests: this.state.guests.map((guest, index) => {
                if (id === guest.id) {
                    return {
                        ...guest,
                        name: name
                    };
                }
                return guest;
            })
        });
    }

    //get attending guests method
    setAttending = () => {
        return this.state.guests.reduce((total, guest)=>{
            return guest.isConfirmed ? total + 1 : total;
        }, 0);
    }

    render() {
        const numberAttending = this.setAttending();
        const guestsUnconfirmed = this.state.guests.length - this.setAttending();
        return (
            <div className="App">
                <Header 
                    pendingGuest={this.state.pendingGuest}
                    updatePendingGuest={this.updatePendingGuest}
                    addGuest={this.addGuest}
                />
                <div className="main">
                    <AttendingFilter 
                        toggleFilter={this.toggleFilter}
                        isFiltered={this.state.isFiltered} 
                    />
                    <Counter 
                        attendingGuests={numberAttending}
                        unconfirmedGuests={guestsUnconfirmed}
                        totalInvited={this.state.guests.length}
                    />   
                    <GuestList 
                        guests={this.state.guests} 
                        toggleConfirmationAt={this.toggleConfirmationAt}
                        toggleEditingAt={this.toggleEditingAt}
                        setNameAt={this.setNameAt}
                        isFiltered={this.state.isFiltered}
                        removeGuest={this.removeGuest}
                        pendingGuest={this.state.pendingGuest}
                        />
                    </div>
                </div>

        );
    }
}

export default App;