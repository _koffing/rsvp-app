import React from 'react';
import PropTypes from 'prop-types';

const Counter = (props) => {
    return (
        <table className="counter">
            <tbody>
                <tr>
                    <td>Attending:</td>
                    <td>{props.attendingGuests}</td>
                </tr>
                <tr>
                    <td>Unconfirmed:</td>
                    <td>{props.unconfirmedGuests}</td>
                </tr>
                <tr>
                    <td>Total:</td>
                    <td>{props.totalInvited}</td>
                </tr>
            </tbody>
        </table>
    );
}

Counter.prototype = {
    attendingGuests: PropTypes.number,
    unconfirmedGuests: PropTypes.number,
    totalInvited: PropTypes.number,
}

export default Counter;